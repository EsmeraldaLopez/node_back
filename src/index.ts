import express from 'express'
import bodyParser from 'body-parser';

const app = express()

app.use(bodyParser.json()); // body en formato json
app.use(bodyParser.urlencoded({ extended: true })); //body formulario
app.use((_req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});



const PORT = 3000

app.post('/ping', (req, res)=>{
    console.log(req.body)
    console.log(req.header)
    res.send(req.body)
})

app.listen(PORT, () =>{
    console.log(`Server running on port ${PORT}`)
})